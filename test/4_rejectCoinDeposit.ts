import { ethers } from "hardhat";
import { getUnixTime } from 'date-fns';
import {expect} from "chai";

describe("DailyVesting", function () {
  let account1: any;

  let dailyVestingContractFactory: any;

  // Initial setup
  const dateFrom = getUnixTime(new Date('2022/01/01'));
  const dateTo = getUnixTime(new Date('2022/12/31'));

  let ERC20Mock: any;
  let DailyVesting: any;

  before(async function () {
    [account1] = await ethers.getSigners();

    dailyVestingContractFactory = await ethers.getContractFactory("DailyVesting");
    const erc20MockContractFactory = await ethers.getContractFactory("ERC20Mock");

    ERC20Mock = await erc20MockContractFactory.deploy(219000);
    await ERC20Mock.deployed();

    DailyVesting = await dailyVestingContractFactory.deploy(
        dateFrom, dateTo, ERC20Mock.address
    )
    await DailyVesting.deployed();
  });

  describe("Reject coin deposit", function () {
    it("Should reject coin deposit", async function () {
      await expect(
          account1.sendTransaction({
            to: DailyVesting.address,
            value: ethers.utils.parseEther("1") // 1 ether
          })
      ).to.be.reverted
    });
  });
});
