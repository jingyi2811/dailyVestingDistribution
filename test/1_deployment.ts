import { ethers } from "hardhat";
import { getUnixTime } from 'date-fns';
import {expect} from "chai";

describe("DailyVesting", function () {
  let deployer: any;

  // Initial setup
  const dateFrom = getUnixTime(new Date('2022/01/01'));
  const dateTo = getUnixTime(new Date('2022/12/31'));

  let ERC20Mock: any;
  let DailyVesting: any;

  before(async function () {
    [deployer] = await ethers.getSigners();

    const dailyVestingContractFactory = await ethers.getContractFactory("DailyVesting");
    const erc20MockContractFactory = await ethers.getContractFactory("ERC20Mock");

    ERC20Mock = await erc20MockContractFactory.deploy(219000);
    await ERC20Mock.deployed();

    DailyVesting = await dailyVestingContractFactory.deploy(
        dateFrom, dateTo, ERC20Mock.address
    )
    await DailyVesting.deployed();
  });

  describe("Deployment", function () {
    it("Should set the right owner", async function () {
      expect(await DailyVesting._admin()).to.equal(deployer.address);
    });

    it("Should set the right date from", async function () {
      expect(await DailyVesting._dateFrom()).to.equal(dateFrom);
    });

    it("Should set the right day to pay", async function () {
      expect(await DailyVesting._dayToPay()).to.equal(
          365
      );
    });

    it("Should have balance", async function () {
      expect(await ERC20Mock.balanceOf(deployer.address)).to.equal(
          '219000000000000000000000'
      );
    });
  });
});