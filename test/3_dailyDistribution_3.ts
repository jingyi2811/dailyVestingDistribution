import { ethers } from "hardhat";
import { getUnixTime } from 'date-fns';
import {expect} from "chai";

describe("DailyVesting", function () {
  let deployer: any;
  let account1: any;
  let account2: any;
  let account3: any;

  let dailyVestingContractFactory: any;

  // Initial setup
  const dateFrom = getUnixTime(new Date('2022/01/01'));
  const dateTo = getUnixTime(new Date('2023/06/30'));

  let ERC20Mock: any;
  let DailyVesting: any;

  before(async function () {
    [deployer, account1, account2, account3] = await ethers.getSigners();

    dailyVestingContractFactory = await ethers.getContractFactory("DailyVesting");
    const erc20MockContractFactory = await ethers.getContractFactory("ERC20Mock");

    ERC20Mock = await erc20MockContractFactory.deploy(1); // 1 BNB is far enough
    await ERC20Mock.deployed();

    DailyVesting = await dailyVestingContractFactory.deploy(
        dateFrom, dateTo, ERC20Mock.address
    )
    await DailyVesting.deployed();
  });

  describe("Daily distribution", function () {
    it("Should distribute", async function () {
      const eighteen_decimal_fractional_part = "000000000000000000";

      await DailyVesting.setRecipientAddressesAmounts(
          [
            account1.address,
            account2.address
          ],
          [
            "123456789",
            "987654321"
          ]
      );

      // 1111111111, which is > than 1111111110 (The sum of 123456789 and 987654321)
      await ERC20Mock.connect(deployer).transfer(DailyVesting.address, "1111111111")
      await DailyVesting.confirmAddressAndTransferOwnership();

      {
        // Before any transfer made
        const balance = await ERC20Mock.balanceOf(account1.address)
        expect(ethers.utils.formatEther(balance)).to.equal("0.0");

        {
          const dailyAmount = await DailyVesting._recipientAddressTotalAmount(account1.address)
          expect(ethers.utils.formatUnits(dailyAmount, 0)).to.equal("123456789");

          const leftAmount = await DailyVesting._recipientAddressLeftAmount(account1.address)
          expect(ethers.utils.formatUnits(leftAmount, 0)).to.equal("123456789");
        }

        {
          const dailyAmount = await DailyVesting._recipientAddressTotalAmount(account2.address)
          expect(ethers.utils.formatUnits(dailyAmount, 0)).to.equal("987654321");

          const leftAmount = await DailyVesting._recipientAddressLeftAmount(account2.address)
          expect(ethers.utils.formatUnits(leftAmount, 0)).to.equal("987654321");
        }

        const dayToPay = await DailyVesting._dayToPay()
        expect(dayToPay).to.equal("546");
      }

      {
        // 2nd day
        const day = 2;

        const time = dateFrom + 86400 * (day - 1)
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await DailyVesting.connect(account1).dailyTransfer()
        await DailyVesting.connect(account2).dailyTransfer()

        {
          const balance = await ERC20Mock.balanceOf(account1.address)
          expect(ethers.utils.formatUnits(balance, 0)).to.equal((226111 * day).toString());
        }

        {
          const balance = await ERC20Mock.balanceOf(account2.address)
          expect(ethers.utils.formatUnits(balance, 0)).to.equal((1808890 * day).toString());
        }
      }

      {
        // 50th day
        const day = 50;

        const time = dateFrom + 86400 * (day - 1)
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await DailyVesting.connect(account1).dailyTransfer()
        await DailyVesting.connect(account2).dailyTransfer()

        {
          const balance = await ERC20Mock.balanceOf(account1.address)
          expect(ethers.utils.formatUnits(balance, 0)).to.equal((226111 * day).toString());
        }

        {
          const balance = await ERC20Mock.balanceOf(account2.address)
          expect(ethers.utils.formatUnits(balance, 0)).to.equal((1808890 * day).toString());
        }
      }

      {
        // 200th day
        const day = 200;

        const time = dateFrom + 86400 * (day - 1)
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await DailyVesting.connect(account1).dailyTransfer()
        await DailyVesting.connect(account2).dailyTransfer()

        {
          const balance = await ERC20Mock.balanceOf(account1.address)
          expect(ethers.utils.formatUnits(balance, 0)).to.equal((226111 * day).toString());
        }

        {
          const balance = await ERC20Mock.balanceOf(account2.address)
          expect(ethers.utils.formatUnits(balance, 0)).to.equal((1808890 * day).toString());
        }
      }

      {
        // 545th day
        const day = 545;

        const time = dateFrom + 86400 * (day - 1)
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await DailyVesting.connect(account1).dailyTransfer()
        await DailyVesting.connect(account2).dailyTransfer()

        {
          const balance = await ERC20Mock.balanceOf(account1.address)
          expect(ethers.utils.formatUnits(balance, 0)).to.equal((226111 * day).toString());
        }

        {
          const balance = await ERC20Mock.balanceOf(account2.address)
          expect(ethers.utils.formatUnits(balance, 0)).to.equal((1808890 * day).toString());
        }
      }

      {
        // 547th day
        const day = 547;

        const time = dateFrom + 86400 * (day - 1)
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await DailyVesting.connect(account1).dailyTransfer()
        await DailyVesting.connect(account2).dailyTransfer()

        {
          const balance = await ERC20Mock.balanceOf(account1.address)
          expect(ethers.utils.formatUnits(balance, 0)).to.equal((226111 * 546).toString());
        }

        {
          const balance = await ERC20Mock.balanceOf(account2.address)
          expect(ethers.utils.formatUnits(balance, 0)).to.equal((1808890 * 546).toString());
        }
      }

      {
        // 600th day
        const day = 600;

        const time = dateFrom + 86400 * (day - 1)
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await DailyVesting.connect(account1).dailyTransfer()
        await DailyVesting.connect(account2).dailyTransfer()

        {
          const balance = await ERC20Mock.balanceOf(account1.address)
          expect(ethers.utils.formatUnits(balance, 0)).to.equal((226111 * 546).toString());
        }

        {
          const balance = await ERC20Mock.balanceOf(account2.address)
          expect(ethers.utils.formatUnits(balance, 0)).to.equal((1808890 * 546).toString());
        }
      }

      // Check leftover balance
      {
        const balance = await ERC20Mock.balanceOf(DailyVesting.address)
        expect(ethers.utils.formatUnits(balance, 0)).to.equal((183 + 381 + 1).toString());
      }
    });
  });
});