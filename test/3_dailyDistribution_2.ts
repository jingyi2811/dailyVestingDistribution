import { ethers } from "hardhat";
import { getUnixTime } from 'date-fns';
import {expect} from "chai";

describe("DailyVesting", function () {
  let deployer: any;
  let account1: any;
  let account2: any;
  let account3: any;

  let dailyVestingContractFactory: any;

  // Initial setup
  const dateFrom = getUnixTime(new Date('2022/01/01'));
  const dateTo = getUnixTime(new Date('2023/12/31'));

  let ERC20Mock: any;
  let DailyVesting: any;

  before(async function () {
    [deployer, account1, account2, account3] = await ethers.getSigners();

    dailyVestingContractFactory = await ethers.getContractFactory("DailyVesting");
    const erc20MockContractFactory = await ethers.getContractFactory("ERC20Mock");

    ERC20Mock = await erc20MockContractFactory.deploy(219000);
    await ERC20Mock.deployed();

    DailyVesting = await dailyVestingContractFactory.deploy(
        dateFrom, dateTo, ERC20Mock.address
    )
    await DailyVesting.deployed();
  });

  describe("Daily distribution", function () {
    it("Should distribute", async function () {
      const eighteen_decimal_fractional_part = "000000000000000000";

      await DailyVesting.setRecipientAddressesAmounts(
          [
            account1.address,
            account2.address,
            account3.address
          ],
          [
            "36500" + eighteen_decimal_fractional_part,
            "73000" + eighteen_decimal_fractional_part,
            "109500" + eighteen_decimal_fractional_part
          ]
      );

      await ERC20Mock.connect(deployer).transfer(DailyVesting.address, "219000" + eighteen_decimal_fractional_part)
      await DailyVesting.confirmAddressAndTransferOwnership();

      {
        // Before any transfer made
        const balance = await ERC20Mock.balanceOf(account1.address)
        expect(ethers.utils.formatEther(balance)).to.equal("0.0");

        const dailyAmount = await DailyVesting._recipientAddressTotalAmount(account1.address)
        expect(ethers.utils.formatEther(dailyAmount)).to.equal("36500.0");

        const leftAmount = await DailyVesting._recipientAddressLeftAmount(account1.address)
        expect(ethers.utils.formatEther(leftAmount)).to.equal("36500.0");

        const dayToPay = await DailyVesting._dayToPay()
        expect(dayToPay).to.equal("730");
      }

      {
        // 2nd day
        const time = dateFrom + 86400 * (2 - 1) // Set to 2 days later
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await DailyVesting.connect(account1).dailyTransfer()

        const balance = await ERC20Mock.balanceOf(account1.address)
        expect(ethers.utils.formatEther(balance)).to.equal("100.0");
      }

      {
        // 5th day
        const time = dateFrom + 86400 * (5 - 1) // Set to 5 days later
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await DailyVesting.connect(account1).dailyTransfer()

        const balance = await ERC20Mock.balanceOf(account1.address)
        expect(ethers.utils.formatEther(balance)).to.equal("250.0");
      }

      {
        // 200th day
        const time = dateFrom + 86400 * (200 - 1) // Set to 200 days later
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await DailyVesting.connect(account1).dailyTransfer()

        const balance = await ERC20Mock.balanceOf(account1.address)
        expect(ethers.utils.formatEther(balance)).to.equal("10000.0");
      }

      {
        // 364th day
        const time = dateFrom + 86400 * (364 - 1) // Set to 364 days later
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await DailyVesting.connect(account1).dailyTransfer()

        const balance = await ERC20Mock.balanceOf(account1.address)
        expect(ethers.utils.formatEther(balance)).to.equal("18200.0");
      }

      {
        // 366th day
        const time = dateFrom + 86400 * (366 - 1) // Set to 366 days later
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await DailyVesting.connect(account1).dailyTransfer()

        const balance = await ERC20Mock.balanceOf(account1.address)
        expect(ethers.utils.formatEther(balance)).to.equal("18300.0");
      }

      {
        // 500th day
        const time = dateFrom + 86400 * (500 - 1) // Set to 500 days later
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await DailyVesting.connect(account1).dailyTransfer()

        const balance = await ERC20Mock.balanceOf(account1.address)
        expect(ethers.utils.formatEther(balance)).to.equal("25000.0");
      }

      {
        // 729th day
        const time = dateFrom + 86400 * (729 - 1) // Set to 729 days later
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await DailyVesting.connect(account1).dailyTransfer()

        const balance = await ERC20Mock.balanceOf(account1.address)
        expect(ethers.utils.formatEther(balance)).to.equal("36450.0");
      }

      {
        // 731th day
        const time = dateFrom + 86400 * (731 - 1) // Set to 731 days later
        await ethers.provider.send('evm_setNextBlockTimestamp', [time]);
        await DailyVesting.connect(account1).dailyTransfer()

        const balance = await ERC20Mock.balanceOf(account1.address)
        expect(ethers.utils.formatEther(balance)).to.equal("36500.0"); // Not 36550
      }
    });
  });
});