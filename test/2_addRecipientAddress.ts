import { ethers } from "hardhat";
import { getUnixTime } from 'date-fns';
import { expect } from "chai";
import { BigNumber } from "ethers";

describe("DailyVesting", function () {
  let deployer: any;
  let account1: any;
  let account2: any;
  let account3: any;

  // Initial setup
  const dateFrom = getUnixTime(new Date('2022/01/01'));
  const dateTo = getUnixTime(new Date('2022/12/31'));

  let ERC20Mock: any;
  let DailyVesting: any;

  beforeEach(async function () {
    [deployer, account1, account2, account3] = await ethers.getSigners();

    const dailyVestingContractFactory = await ethers.getContractFactory("DailyVesting");
    const erc20MockContractFactory = await ethers.getContractFactory("ERC20Mock");

    ERC20Mock = await erc20MockContractFactory.deploy(219000);
    await ERC20Mock.deployed();

    DailyVesting = await dailyVestingContractFactory.deploy(
        dateFrom, dateTo, ERC20Mock.address
    )
    await DailyVesting.deployed();
  });

  describe("Set recipient addresses", function () {
    it("Should set recipient addresses 1", async function () {
      expect(await DailyVesting._hasAddressNotYetConfirmed()).to.equal(true);

      const eighteen_decimal_fractional_part = "000000000000000000";

      await DailyVesting.setRecipientAddressesAmounts(
          [
              account1.address,
              account2.address,
              account3.address
          ],
          [
              "36500" + eighteen_decimal_fractional_part,
              "73000" + eighteen_decimal_fractional_part,
              "109500" + eighteen_decimal_fractional_part
          ]
      );

      {
        let account1Balance: BigNumber = await DailyVesting._recipientAddressTotalAmount(account1.address)
        expect(ethers.utils.formatEther(account1Balance)).to.equal("36500.0");

        let account2Balance: BigNumber = await DailyVesting._recipientAddressTotalAmount(account2.address)
        expect(ethers.utils.formatEther(account2Balance)).to.equal("73000.0");

        let account3Balance: BigNumber = await DailyVesting._recipientAddressTotalAmount(account3.address)
        expect(ethers.utils.formatEther(account3Balance)).to.equal("109500.0");
      }

      {
        let account1Balance: BigNumber = await DailyVesting._recipientAddressLeftAmount(account1.address)
        expect(ethers.utils.formatEther(account1Balance)).to.equal("36500.0");

        let account2Balance: BigNumber = await DailyVesting._recipientAddressLeftAmount(account2.address)
        expect(ethers.utils.formatEther(account2Balance)).to.equal("73000.0");

        let account3Balance: BigNumber = await DailyVesting._recipientAddressLeftAmount(account3.address)
        expect(ethers.utils.formatEther(account3Balance)).to.equal("109500.0");
      }

      await ERC20Mock.connect(deployer).transfer(DailyVesting.address, "219000" + eighteen_decimal_fractional_part)

      await DailyVesting.confirmAddressAndTransferOwnership();
    });

    it("Should set recipient addresses 2", async function () {
      expect(await DailyVesting._hasAddressNotYetConfirmed()).to.equal(true);

      const eighteen_decimal_fractional_part = "000000000000000000";

      await DailyVesting.setRecipientAddressesAmounts(
          [
            account1.address,
            account2.address,
            account3.address
          ],
          [
            "18250" + eighteen_decimal_fractional_part,
            "21900" + eighteen_decimal_fractional_part,
            "25550" + eighteen_decimal_fractional_part
          ]
      );

      {
        let account1Balance: BigNumber = await DailyVesting._recipientAddressTotalAmount(account1.address)
        expect(ethers.utils.formatEther(account1Balance)).to.equal("18250.0");

        let account2Balance: BigNumber = await DailyVesting._recipientAddressTotalAmount(account2.address)
        expect(ethers.utils.formatEther(account2Balance)).to.equal("21900.0");

        let account3Balance: BigNumber = await DailyVesting._recipientAddressTotalAmount(account3.address)
        expect(ethers.utils.formatEther(account3Balance)).to.equal("25550.0");
      }

      {
        let account1Balance: BigNumber = await DailyVesting._recipientAddressLeftAmount(account1.address)
        expect(ethers.utils.formatEther(account1Balance)).to.equal("18250.0");

        let account2Balance: BigNumber = await DailyVesting._recipientAddressLeftAmount(account2.address)
        expect(ethers.utils.formatEther(account2Balance)).to.equal("21900.0");

        let account3Balance: BigNumber = await DailyVesting._recipientAddressLeftAmount(account3.address)
        expect(ethers.utils.formatEther(account3Balance)).to.equal("25550.0");
      }

      await ERC20Mock.connect(deployer).transfer(DailyVesting.address, "65700" + eighteen_decimal_fractional_part)

      await DailyVesting.confirmAddressAndTransferOwnership();
    });
  });
});
